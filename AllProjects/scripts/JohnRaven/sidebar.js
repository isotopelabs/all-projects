$(function () {
    $("#sidebarToggle").click(function (e) {
        e.preventDefault();
        $("#sidebar").toggleClass("toggleMe");
        $("#sidebarToggle").toggleClass("toggleMeSidebar");
    });
});